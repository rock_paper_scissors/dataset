#### Background Information
The dataset directory contains the numerical measurements of different physical and functional properties observed in different object instances.  
The measurements have been estimated using different methodologies in both the datasets. 

#### Structure
The project contains three directories: <mark>RoCS</mark>, <mark>RoCS_Objects</mark>, <mark>Washington_Dataset-based</mark>.


##### RoCS
<mark>RoCS</mark> stands for **Ro**bot-**C**entric data**S**et. The dataset consists of 11 different object classes where each class consists of 10 unique object instances that leads to a total number of 110 object instances. 
The dataset contains the numerical measurements of six physical and four functional properties estimated from each of the 110 object instances.
The six physical properties are: *hollowness*, *flatness*, *size*, *rigidity*, *roughess*, *heaviness*.
The four functional properties are: *containment*, *support*, *movability*, *blockage*.

We developed light-weight estimation methods[^1] which were used to estimate the measurements of the aforementioned properties. Each property was measured in terms of certain features extracted from the sensory data. The figure below illustrates which sensors were used, what features were extracted for each property.
Note that, the functional properties were measured in terms of the physical properties which enable them. Depending on how a property is
computationally defined, a measurement is either a scalar value or a vector.

![Property Measurements](https://drive.google.com/uc?export=view&id=1vVeNYQ8sN63p_P3XB86_AdJCRLtQCSQR 'Features used to measure properties')

##### RoCS_Objects
<mark>RoCS_Objects</mark> contains the images of the objects that were used to generate the RoCS dataset. For the RoCS dataset we consider 11 different object classes: *ball*, *book*, *bowl*, *cup*, *metal_box*, *paper_box*, *plastic_box*, *plate*, *sponge*, *to_go_cup*, *tray*. Each class consists of 10 unique object instances that leads to a total number of 110 object instances; The figure below illustrates sample object instances of each object class of RoCS dataset.

![Example Objects](https://drive.google.com/uc?export=view&id=1fYrIzD-sygBHZoSQgZS0LHT6BuEtOICO 'Sample Object Instances')


##### Washington_Dataset-based 
<mark>Washington_Dataset-based</mark> contains the property measurements of 692 object instances. To generate the dataset, we have used household object images from the RGB-D Washington Dataset[^2]. The Washington dataset contains total 300 everyday object instances covering 51 object categories where each object instance is captured from multiple view angles that leads to total 250,000 RGB-D images[^3] . There are primarily four super object categories: fruits, vegetables, devices, and containers. For this experiment, we targeted devices and containers super categories from which 22 object classes were selected and for each class, we selected random images from all the given object instances of the class leading up to total of 692 images[^4]. We generated this dataset using the composite of a machine-centric and a human-centric method. The figure below illustrates the number of images per instance and the number of instances selected from each class.

![Sample Objects](https://drive.google.com/uc?export=view&id=1NL6ZHV6e7x1Ac96atnYa5Y_--plKxPad 'Sample Object Instances from Washington Dataset')

###### Dataset Generation - Machine-centric Method
In the machine-centric approach, geometrical properties were acquired using a state-of-art non-invasive object shape learning technique[^5] which in a data-driven and unsupervised manner learns shape concepts from RGB-D object point clouds as shown in the figure below. In total, 58 geometrical properties or shape concepts were generated using the unsupervised approach from which four shape concepts were selected using a baseline feature selection technique Variance Threshold. We limited the number of shape concepts to four in order not to skew the relevant properties of a missing tool and by extension substitute selection in favor of the shape concepts. The learned shape concepts for the objects are used in the knowledge as machine-generated geometric object properties and are denoted as *concept0*, *concept1*, *concept2*, *concept3* in the directory.

![Machine-centric Concepts](https://drive.google.com/uc?export=view&id=1GEVgl5Z_7FmP5rLnuxCxyyAqEMqx8Hmi 'Approach to generate Machine-centric Concepts')

###### Dataset Generation - Human-centric Method
For human-centric method, we considered non-geometrical physical properties such as *weight*, *rigidity*, *hollowness* and the functional properties like *support*, *blockage* and *containment*. These properties were synthetically acquired by sampling from human expert knowledge. For each object class, based
on the selected object instances, the measurement distribution of each property was approximated by drawing random samples from a normal (Gaussian) distribution. The distribution for each property was generated by providing the mean and standard deviation for each class, for the given number of images of each class where the mean and the standard deviation was provided by a human expert. We generated the dataset in Python using NumPy random normal function generator 4 given by,  
```
numpy.random.normal(loc=0.0, scale=1.0, size=None)
```
where *loc* is Mean of the distribution, *scale* is Standard Deviation of the distribution and *size* is the shape of the output array, in our case, it will
be the number of instances selected for each class. We have provided the Python files that were used to generate the measurements using this method. The files are <mark>definition_data.py</mark> and <mark>generate_data.py</mark>.  
In order to generate the human-centric measurements, run the following command on a command line:  
```
python generate_data.py
```

---

[^1]: see our journal article detailing the methods: Thosar, M., Mueller, C. A., Jaeger, G., Schleiss, J., Pulugu, N., Chennaboina, R. M., … Zug, S. (2021). From Multi-modal Property Dataset to Robot-Centric Conceptual Knowledge About Household Objects. Frontiers in Robotics and AI - Computational Intelligence in Robotics, Special Issue: Robots That Learn and Reason: Towards Learning Logic Rules from Noisy Data, 8, 87.

[^2]: K. Lai, L. Bo, X. Ren, and D. Fox, “A Large-Scale Hierarchical Multi-View RGB-D Object Dataset,” in IEEE International Conference on Robotics and Automation (ICRA), Shanghai, China, 2011, pp. 1817–1824.

[^3]: <https://rgbd-dataset.cs.washington.edu/dataset/>

[^4]: <https://rgbd-dataset.cs.washington.edu/dataset/rgbd-dataset/>

[^5]: C. A. Mueller and A. Birk, “Conceptualization of Object Compositions Using Persistent Homology,” in IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS), Madrid, 2018.
