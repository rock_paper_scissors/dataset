import argparse
import random
import os
import os.path
import numpy

# ball, bowl, plate, cap, cereal_box, coffee mug, flash_light, binder, food box, food bag, food can, food cup, food jar,
# keyboard, kleenex, sponge, hand towel, notebook, pitcher, shampoo bottle, soda can, water bottle

def make_weight(obj_name, mean, deviation, size):
    make_data(obj_name, "weight", mean, deviation, size)


def make_rigid(obj_name, mean, deviation, size):
    make_data(obj_name, "rigid", mean, deviation, size)

def make_hollow(obj_name, mean, deviation, size):
    make_data(obj_name, "hollow", mean, deviation, size)

def make_support(obj_name, mean, deviation, size):
    make_data(obj_name, "support", mean, deviation, size)

def make_blockage(obj_name, mean, deviation, size):
    make_data(obj_name, "blockage", mean, deviation, size)

def make_containment(obj_name, mean, deviation, size):
    make_data(obj_name, "containment", mean, deviation, size)


def make_data(obj_name, prop_name, mean, deviation, size):
    num = numpy.random.normal(mean, deviation, size)
    write_property_data(obj_name, prop_name, num)
            


def write_property_data(obj_name, prop_name, data):
    prop_file = prop_name+".txt"# + "_" + str(num)
    if os.path.isfile(prop_file):
        with open(prop_file, "a+") as append_data:
            for i, val in enumerate(data):
                append_data.write(obj_name+'_'+str(i)+" "+str(val)+"\n")
        append_data.close()
    else:
        with open(prop_file, "w") as write_data:
            for i, val in enumerate(data):
                write_data.write((obj_name+'_'+str(i)+" "+str(val)+"\n"))
        write_data.close()